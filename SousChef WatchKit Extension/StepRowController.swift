//
//  StepRowController.swift
//  SousChef
//
//  Created by Brandon Levasseur on 4/15/15.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import WatchKit

class StepRowController: NSObject {
   
  @IBOutlet weak var stepLabel: WKInterfaceLabel!
  @IBOutlet weak var directionsLabel: WKInterfaceLabel!
  
}
