//
//  RecipeRowController.swift
//  SousChef
//
//  Created by Brandon Levasseur on 4/14/15.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import WatchKit

class RecipeRowController: NSObject {
  @IBOutlet weak var textLabel: WKInterfaceLabel!
  @IBOutlet weak var ingredientsLabel: WKInterfaceLabel!
}