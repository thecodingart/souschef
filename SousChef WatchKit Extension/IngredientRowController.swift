//
//  IngredientRowController.swift
//  SousChef
//
//  Created by Brandon Levasseur on 4/15/15.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import WatchKit

class IngredientRowController: NSObject {
   
  @IBOutlet weak var nameLabel: WKInterfaceLabel!
  @IBOutlet weak var measurementLabel: WKInterfaceLabel!
}
