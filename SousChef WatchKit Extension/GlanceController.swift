//
//  GlanceController.swift
//  SousChef
//
//  Created by Brandon Levasseur on 4/28/15.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import WatchKit
import SousChefKit

class GlanceController: WKInterfaceController {
  
  @IBOutlet weak var statusLabel: WKInterfaceLabel!
  @IBOutlet weak var upNextLabel: WKInterfaceLabel!
  @IBOutlet weak var onDeckLabel: WKInterfaceLabel!
  
  var upNextItem: Ingredient?
  var onDeckItem: Ingredient?
  
  let groceryList = GroceryList().flattenedGroceries()
  
  override func willActivate() {
    let items = groceryList.filter {
      $0.item is Ingredient
      }.map {
        $0.item as! Ingredient
    }
    
    let notPurchased = items.filter {
      !$0.purchased
    }
    
    let purchasedCount = items.count - notPurchased.count
    statusLabel.setText("\(purchasedCount)/\(items.count)")
    
    if notPurchased.count > 0 {
      upNextItem = notPurchased.first
      upNextLabel.setText(upNextItem!.name.capitalizedString)
    }
    
    if notPurchased.count > 1 {
      onDeckItem = notPurchased[1]
      onDeckLabel.setText(onDeckItem!.name.capitalizedString)
    }
    
    if let upNextItem = upNextItem {
      updateUserActivity(kGlanceHandoffActivityName, userInfo: [kHandoffVersionKey: kHandoffVersionNumber, kGlanceHandoffNextItemKey: upNextItem.name], webpageURL: nil)
    }
  }
  
  override func didDeactivate() {
    super.didDeactivate()
    invalidateUserActivity()
  }
  
}
