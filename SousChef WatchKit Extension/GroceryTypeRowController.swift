//
//  GroceryTypeRowController.swift
//  SousChef
//
//  Created by Brandon Levasseur on 4/17/15.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import WatchKit

class GroceryTypeRowController: NSObject {
   
  @IBOutlet weak var image: WKInterfaceImage!
  @IBOutlet weak var textLabel: WKInterfaceLabel!
  
}
