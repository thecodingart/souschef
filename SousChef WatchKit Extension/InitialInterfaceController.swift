//
//  InitialInterfaceController.swift
//  SousChef
//
//  Created by Ryan Nystrom on 11/26/14.
//  Copyright (c) 2014 Ray Wenderlich. All rights reserved.
//

import WatchKit
import SousChefKit

class InitialInterfaceController: WKInterfaceController {
  
  let recipeStore = RecipeStore()
  let fileManager = NSFileManager.defaultManager()
  let groceryListQuery = NSMetadataQuery()
  
  override func willActivate() {
    super.willActivate()
    
    if /*let currentToken = fileManager.ubiquityIdentityToken*/let iCloudContainerURL = self.fileManager.URLForUbiquityContainerIdentifier(GroceryListConfig.iCloudID) {
      println("iCloud access with ID \(iCloudContainerURL)")
      queryGroceryListCloudContainer()
    } else {
      println("No iCloud access")
      setupGroceryListGroupDoc()
    }
  }
  
  override func handleUserActivity(userInfo: [NSObject : AnyObject]?) {
    println("Received a Handoff payload: \(userInfo)")
    
    if let version = userInfo![kHandoffVersionKey] as? String where version == kHandoffVersionNumber, let nextItem = userInfo![kGlanceHandoffNextItemKey] as? String {
      self.pushControllerWithName("GroceryController", context: nextItem)
    }
  }
  
  override func handleActionWithIdentifier(identifier: String?, forLocalNotification localNotification: UILocalNotification) {
    if let userInfo = localNotification.userInfo {
      processActionWithIdentifier(identifier, withUserInfo: userInfo)
    }
  }
  
  override func handleActionWithIdentifier(identifier: String?, forRemoteNotification remoteNotification: [NSObject : AnyObject]) {
    processActionWithIdentifier(identifier, withUserInfo: remoteNotification)
  }
  
  func processActionWithIdentifier(identifer: String?, withUserInfo userInfo: [NSObject: AnyObject]) {
    if let title = userInfo["title"] as? String where identifer == "viewDirectionsButtonAction" {
      let matchingRecipes = recipeStore.recipes.filter {
        $0.name == title
      }
      pushControllerWithName("RecipeDirections", context: matchingRecipes.first!)
    }
  }
  
  func createNewGroceryListDoc() {
    let newGroceryListDoc = GroceryList(fileURL: GroceryListConfig.url)
    newGroceryListDoc.saveToURL(newGroceryListDoc.fileURL, forSaveOperation: UIDocumentSaveOperation.ForCreating) { success in
      if success {
        println("createNewGroceryListDoc: success")
      } else {
        println("createNewGroceryListDoc: failed")
      }
    }
  }
  
  func setupGroceryListGroupDoc() {
    GroceryListConfig.url = GroceryListConfig.groupURL
    //check for existing doc; create one if none exist
    if !fileManager.fileExistsAtPath(GroceryListConfig.url.path!) {
      println("\(__FUNCTION__): create empty group doc")
      createNewGroceryListDoc()
    }
  }
  
  
  func setupGroceryListCloudDoc(query: NSMetadataQuery) {
    if query.resultCount > 0 {
      let item = query.resultAtIndex(0) as! NSMetadataItem
      let groceryListCloudURL = item.valueForAttribute(NSMetadataItemURLKey) as! NSURL
      GroceryListConfig.cloudURL = groceryListCloudURL
      GroceryListConfig.url = GroceryListConfig.cloudURL
      
      if fileManager.fileExistsAtPath(GroceryListConfig.groupURL.path!) {
        fileManager.removeItemAtURL(groceryListCloudURL, error: nil)
        moveGroupDocToCloud()
      }
    } else {
      dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
        if let iCloudContainerURL = self.fileManager.URLForUbiquityContainerIdentifier(GroceryListConfig.iCloudID) {
          println("\(__FUNCTION__): new cloud doc at: \(iCloudContainerURL)")
          let groceryListCloudURL = iCloudContainerURL.URLByAppendingPathComponent("Documents").URLByAppendingPathComponent(GroceryListConfig.filename)
          GroceryListConfig.cloudURL = groceryListCloudURL
          GroceryListConfig.url = GroceryListConfig.cloudURL
          
          if self.fileManager.fileExistsAtPath(GroceryListConfig.groupURL.path!) {
            println("\(__FUNCTION__): moving group doc to cloud")
            self.moveGroupDocToCloud()
          } else {
            println("\(__FUNCTION__): creating empty cloud doc")
            self.createNewGroceryListDoc()
          }
        }
      }
    }
  }
  
  func queryGroceryListCloudContainer() {
    groceryListQuery.searchScopes = [NSMetadataQueryUbiquitousDocumentsScope]
    groceryListQuery.predicate = NSPredicate(format: "(%K = %@)", argumentArray: [NSMetadataItemFSNameKey, GroceryListConfig.filename])
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "metadataQueryDidFinishGathering:", name: NSMetadataQueryDidFinishGatheringNotification, object: groceryListQuery)
    groceryListQuery.startQuery()
  }
  
  @objc private func metadataQueryDidFinishGathering(notification: NSNotification) {
    groceryListQuery.disableUpdates()
    groceryListQuery.stopQuery()
    NSNotificationCenter.defaultCenter().removeObserver(self, name: NSMetadataQueryDidFinishGatheringNotification, object: groceryListQuery)
    setupGroceryListCloudDoc(groceryListQuery)
  }
  
  func moveGroupDocToCloud() {
    let defaultQueue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
    dispatch_async(defaultQueue) {
      let success = self.fileManager.setUbiquitous(true, itemAtURL: GroceryListConfig.groupURL, destinationURL: GroceryListConfig.cloudURL, error: nil)
      
      if success {
        println("\(__FUNCTION__) moved doc to cloud")
        
        let groceryList = GroceryList(fileURL: GroceryListConfig.cloudURL)
        groceryList.openWithCompletionHandler { success in
          if success {
            println("\(__FUNCTION__): opened groceryList")
            groceryList.sync()
          } else {
            println("\(__FUNCTION__): open groceryList failed")
          }
        }
      }
      return
    }
  }

}
