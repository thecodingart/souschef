//
//  HandoffConstants.swift
//  SousChef
//
//  Created by Brandon Levasseur on 4/29/15.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import Foundation

let kHandoffVersionKey = "version"
let kHandoffVersionNumber = "1.0"
let kGlanceHandoffActivityName = "com.tca.couschef.glance"
let kGlanceHandoffNextItemKey = "nextItem"
