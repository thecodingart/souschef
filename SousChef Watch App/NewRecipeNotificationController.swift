//
//  NewRecipeNotificationController.swift
//  SousChef
//
//  Created by Brandon Levasseur on 5/1/15.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import WatchKit
import Foundation


class NewRecipeNotificationController: WKUserNotificationInterfaceController {

  @IBOutlet weak var recipeImage: WKInterfaceImage!
  @IBOutlet weak var titleLabel: WKInterfaceLabel!
  @IBOutlet weak var messageLabel: WKInterfaceLabel!
  @IBOutlet weak var ingredientsLabel: WKInterfaceLabel!
  @IBOutlet weak var ingredientsCountLabel: WKInterfaceLabel!
  @IBOutlet weak var stepsLabel: WKInterfaceLabel!
  @IBOutlet weak var stepsCountLabel: WKInterfaceLabel!
  
  override func didReceiveRemoteNotification(remoteNotification: [NSObject : AnyObject], withCompletion completionHandler: (WKUserNotificationInterfaceType) -> Void) {
    if let apsDictionary = remoteNotification["aps"] as? NSDictionary, alertDictionary = apsDictionary["alert"] as? NSDictionary {
      
      if let message = alertDictionary["body"] as? String {
        messageLabel.setHidden(false)
        messageLabel.setText(message)
      } else {
        messageLabel.setHidden(true)
      }
      
      if let name = alertDictionary["title"] as? String {
        titleLabel.setHidden(false)
        titleLabel.setText(name)
      } else {
        titleLabel.setHidden(true)
      }
      
      if let ingredients = remoteNotification["ingredients"] as? String {
        ingredientsLabel.setHidden(false)
        ingredientsCountLabel.setHidden(false)
        ingredientsCountLabel.setText(ingredients)
      } else {
        ingredientsLabel.setHidden(true)
        ingredientsCountLabel.setHidden(true)
      }
      
      if let steps = remoteNotification["steps"] as? String {
        stepsLabel.setHidden(false)
        stepsCountLabel.setHidden(false)
        stepsCountLabel.setText("3")
      } else {
        stepsLabel.setHidden(true)
        stepsCountLabel.setHidden(true)
      }
      
      if let imageURLString = remoteNotification["imageURL"] as? String, imageURL = NSURL(string: imageURLString) {
        let request = NSURLRequest(URL: imageURL, cachePolicy: .UseProtocolCachePolicy, timeoutInterval: 1)
        NSURLSession.sharedSession().dataTaskWithRequest(request) { data, response, error in
          dispatch_async(dispatch_get_main_queue()) {
            if let httpResponse = response as? NSHTTPURLResponse where httpResponse.statusCode == 200 {
              self.recipeImage.setHidden(false)
              self.recipeImage.setImageData(data)
            } else {
              println("Error with the request")
              self.recipeImage.setHidden(true)
            }
          }
          
          completionHandler(.Custom)
        }.resume()
      }
    }
  }
}
