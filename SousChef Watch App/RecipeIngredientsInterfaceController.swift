//
//  RecipeIngredientsInterfaceController.swift
//  SousChef
//
//  Created by Brandon Levasseur on 4/15/15.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import WatchKit
import Foundation
import SousChefKit


class RecipeIngredientsInterfaceController: WKInterfaceController {
  
  @IBOutlet weak var table: WKInterfaceTable!
  let groceryList = GroceryList(fileURL: GroceryListConfig.url)
  var recipe: Recipe?
  
  override func awakeWithContext(context: AnyObject?) {
    super.awakeWithContext(context)
    
    recipe = context as? Recipe
    
    if let ingredients = recipe?.ingredients {
      table.setNumberOfRows(ingredients.count, withRowType: "IngredientRow")
      
      for (index, ingredient) in enumerate(ingredients) {
        let controller = table.rowControllerAtIndex(index) as! IngredientRowController
        
        controller.nameLabel.setText(ingredient.name.capitalizedString)
        controller.measurementLabel.setText(ingredient.quantity)
        
      }
      
    }
  }
  
  override func willActivate() {
    // This method is called when watch view controller is about to be visible to user
    super.willActivate()
  }
  
  override func didDeactivate() {
    // This method is called when watch view controller is no longer visible
    super.didDeactivate()
    groceryList.closeWithCompletionHandler(nil)
  }
  
  @IBAction func onAddToGrocery() {
    groceryList.openWithCompletionHandler { success in
      if !success {
        println("RecipeIngredientsIC: open groceryList failed")
        return
      }
      self.addToGrocery()
    }
  }
  
  func addToGrocery() {
    if let items = recipe?.ingredients {
      for item in items {
        groceryList.addItemToList(item)
      }
      groceryList.sync()
    }
  }
  
}
