//
//  RecipesInterfaceController.swift
//  SousChef
//
//  Created by Brandon Levasseur on 4/14/15.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import WatchKit
import SousChefKit

class RecipesInterfaceController: WKInterfaceController {
  
  @IBOutlet weak var table: WKInterfaceTable!
  let recipeStore = RecipeStore()
  
  override func awakeWithContext(context: AnyObject?) {
    super.awakeWithContext(context)
    updateTable()
  }
  
  override func willActivate() {
    super.willActivate()
    let kGroceryUpdateRequest = "com.thecodingart.update-recipes"
    let requestInfo: [NSObject: AnyObject] = [kGroceryUpdateRequest: true]
    WKInterfaceController.openParentApplication(requestInfo) { replyInfo, error in
      self.updateTable()
    }
  }
  
  override func contextForSegueWithIdentifier(segueIdentifier: String, inTable table: WKInterfaceTable, rowIndex: Int) -> AnyObject? {
    return recipeStore.recipes[rowIndex]
  }
  
  func updateTable() {
    let recipes = recipeStore.recipes
    if table.numberOfRows != recipes.count {
      table.setNumberOfRows(recipes.count, withRowType: "RecipeRowType")
    }
    
    for (index, recipe) in enumerate(recipes) {
      let controller = table.rowControllerAtIndex(index) as! RecipeRowController
      controller.textLabel.setText(recipe.name)
      controller.ingredientsLabel.setText("\(recipe.ingredients.count) ingredients")
    }
  }
}
