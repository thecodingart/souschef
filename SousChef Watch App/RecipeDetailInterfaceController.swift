//
//  RecipeDetailInterfaceController.swift
//  SousChef
//
//  Created by Brandon Levasseur on 4/14/15.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import WatchKit
import SousChefKit

class RecipeDetailInterfaceController: WKInterfaceController {
  @IBOutlet weak var nameLabel: WKInterfaceLabel!
  @IBOutlet weak var nameGroup: WKInterfaceGroup!
  var recipe: Recipe?
  
  override func awakeWithContext(context: AnyObject?) {
    super.awakeWithContext(context)
    recipe = context as? Recipe
    nameLabel.setText(recipe?.name)
    
    if let imageName = recipe?.imageURL?.path?.lastPathComponent {
      let cacheHelper = OnDeviceCacheHelper()
      
      if cacheHelper.cacheContainsImageNamed(imageName) == true {
        nameGroup.setBackgroundImageNamed(imageName)
        
      } else if let imageURL = recipe?.imageURL {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0)) {
          let imageData = NSData(contentsOfURL: imageURL)!
          let recipeImage = UIImage(data: imageData)!
          let retinaRect = CGRect(x: 0, y: 0, width: self.contentFrame.size.width * 2, height: self.contentFrame.size.height * 2)
          let resizedImage = recipeImage.resizedImageWithAspectRatioInsideRect(retinaRect)
          let overlayedImage = resizedImage.imageWithOverlayedColor(UIColor.blackColor().colorWithAlphaComponent(0.3))
          
          dispatch_async(dispatch_get_main_queue()) {
            cacheHelper.addImageToCache(overlayedImage, name: imageName)
            self.nameGroup.setBackgroundImageNamed(imageName)
          }
        }
      }
    }
  }
  
  override func contextForSegueWithIdentifier(segueIdentifier: String) -> AnyObject? {
    return recipe
  }
}