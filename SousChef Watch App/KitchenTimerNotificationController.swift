//
//  KitchenTimerNotificationController.swift
//  SousChef
//
//  Created by Brandon Levasseur on 4/30/15.
//  Copyright (c) 2015 Ray Wenderlich. All rights reserved.
//

import WatchKit

class KitchenTimerNotificationController: WKUserNotificationInterfaceController {
  
  @IBOutlet weak var titleLabel: WKInterfaceLabel!
  @IBOutlet weak var messageLabel: WKInterfaceLabel!
  
  override func didReceiveLocalNotification(localNotification: UILocalNotification, withCompletion completionHandler: (WKUserNotificationInterfaceType) -> Void) {
    if let userInfo = localNotification.userInfo {
      processNotificationWithUserInfo(userInfo, withCompletion: completionHandler)
    }
  }
  
  override func didReceiveRemoteNotification(remoteNotification: [NSObject : AnyObject], withCompletion completionHandler: (WKUserNotificationInterfaceType) -> Void) {
    processNotificationWithUserInfo(remoteNotification, withCompletion: completionHandler)
  }
  
  func processNotificationWithUserInfo(userInfo: [NSObject: AnyObject], withCompletion completionHandler:(WKUserNotificationInterfaceType) -> Void) {
    messageLabel.setHidden(true)
    if let message = userInfo["message"] as? String {
      messageLabel.setHidden(false)
      messageLabel.setText(message)
    }
    
    titleLabel.setHidden(true)
    if let title = userInfo["title"] as? String {
      titleLabel.setHidden(false)
      titleLabel.setText(title)
    }
    
    completionHandler(.Custom)
  }
}
